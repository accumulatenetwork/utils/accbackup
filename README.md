## AccBackup

- Backup and Restore AccMan docker volumes to/from AWS S3 bucket
- Synchronises changes
- Optional host Shutdown after backup


#### Required Environment Variables


    AWS_ACCESS_KEY_ID  
    AWS_SECRET_ACCESS_KEY  
    S3_BUCKET_NAME  //this is typically the volume name, with a unique prefix, like your company name.  


#### Startup aruguments

    backup <shutdown>  
    restore <S3version> <shutdown>   


#### AccMan integration


    SYNCONLY (optional) tells AccMan to Sync Accumulate, start a backup, and shutdown (shutdown -h now).  
    S3_BUCKET_PREFIX is your unique bucket name prefix.  


Create a .env file in the /accman directory.

    CONTAINER_ARGS="--net=host -e SYNCONLY=true -e AWS_ACCESS_KEY_ID=********** -e AWS_SECRET_ACCESS_KEY=*************** -e S3_BUCKET_PREFIX=MyCompany"
