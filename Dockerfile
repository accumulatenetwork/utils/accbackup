# Use the Golang:Alpine image as the base
FROM alpine:latest

ARG COMMIT_HASH
ARG COMMIT_DATE

ENV GIT_COMMIT_HASH $COMMIT_HASH
ENV GIT_COMMIT_DATE $COMMIT_DATE

# Set the working directory
WORKDIR /

# Install the dependencies
RUN apk update && apk add --no-cache aws-cli wget


COPY start.sh /.
COPY backup.sh /.
COPY restore.sh /.
RUN chmod +x start.sh
RUN chmod +x backup.sh
RUN chmod +x restore.sh
# Set the entrypoint
ENTRYPOINT ["/start.sh"]
