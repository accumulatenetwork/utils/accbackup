#!/bin/sh

echo "AccBackup - $GIT_COMMIT_HASH $GIT_COMMIT_DATE"

# Check if the required environmental variables are set
if [ -z "$AWS_ACCESS_KEY_ID" ] || [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
  echo 'Error: One or more of the required environmental variables is not set. Please set AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY and try again.' >&2
  exit 1;
fi

# Check if the required environmental variables are set
if [ -z "$S3_BUCKET_NAME" ]; then
  echo 'Error: Please set S3_BUCKET_NAME, and try again.' >&2
  exit 1
fi


if [ "$1" = "backup" ]; then
  source backup.sh
elif [ "$1" = "restore" ]; then
  source restore.sh $2
else
  echo "You can type in ./backup.sh or ./restore.sh"
  sh
fi


if [[ "$*" == *"shutdown"* ]]; then
  echo "Shutdown requested"
  aws ec2 stop-instances --instance-ids $(wget -q -O - http://169.254.169.254/latest/meta-data/instance-id)  --region $(wget -q -O - http://169.254.169.254/latest/meta-data/placement/region)
fi