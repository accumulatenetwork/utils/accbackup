#!/bin/sh

#AWS_ACCESS_KEY_ID=""
#AWS_SECRET_ACCESS_KEY=""
#S3_BUCKET_PREFIX="MyUniquePrefix"  #e.g companyName


# Get the list of docker volumes
volumes=$(docker volume ls --format '{{.Name}}')

# Find the volume that starts with "acc_" and ends with "_bvn" followed by a number
result=$(echo "$volumes" | grep -E '^acc_.*_bvn[0-9]+$')

# Check if there is exactly one result
if [ $(echo "$result" | wc -l) -ne 1 ]; then
  echo "Error: Found more than one volume that matches the pattern." >&2
  exit 1
fi

# Get the volume name
VOLUME_NAME=$(echo "$result" | awk '{print $1}')
S3_BUCKET_NAME="$(echo "$VOLUME_NAME" | sed 's/_/-/g')"

docker run -it --rm --pull=always \
	 -v $VOLUME_NAME:/data \
	 -e AWS_ACCESS_KEY_ID="$AWS_ACCESS_KEY_ID" \
	 -e AWS_SECRET_ACCESS_KEY="$AWS_SECRET_ACCESS_KEY" \
	 -e S3_BUCKET_NAME="$S3_BUCKET_PREFIX$S3_BUCKET_NAME" \
	 registry.gitlab.com/accumulatenetwork/utils/accbackup $1