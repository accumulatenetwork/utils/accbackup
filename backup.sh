#!/bin/sh

# Define the source and destination directories
src_dir="/data"

# Check if the required environmental variables are set
if [ -z "$S3_BUCKET_NAME" ]; then
  echo ' Please set S3_BUCKET_NAME, and try again.' >&2
  exit 1
fi

S3_BUCKET_NAME=$(echo "$S3_BUCKET_NAME" | sed 's/_/-/g')

# Check if bucket exists
if aws s3api head-bucket --bucket $S3_BUCKET_NAME 2>/dev/null; then
  echo "Bucket $S3_BUCKET_NAME already exists."
else
  echo "Creating bucket $S3_BUCKET_NAME."
  aws s3 mb s3://$S3_BUCKET_NAME
  if [ $? -ne 0 ]; then
    echo 'Error: Failed to create S3 bucket.' >&2
    exit 1
  fi
fi

echo "$src_dir -> $S3_BUCKET_NAME"
echo "AccBackup ($GIT_COMMIT_HASH) - $(date)" >> backup.log
aws s3 sync $src_dir s3://$S3_BUCKET_NAME --exclude backup.log --exclude priv_validator_key.json --exclude register.json --no-progress | tee -a backup.log
echo "$(uptime)" >> backup.log
aws s3 mv backup.log s3://$S3_BUCKET_NAME
