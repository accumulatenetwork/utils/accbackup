#!/bin/sh

# Define the source and destination directories
dest_dir="$MOUNT_POINT"

# Check if the required environmental variables are set
if [ -z "$S3_BUCKET_NAME" ]; then
  echo 'Error: Please set S3_BUCKET_NAME, and try again.' >&2
  exit 1
fi

S3_BUCKET_NAME=$(echo "$S3_BUCKET_NAME" | sed 's/_/-/g')

# Check if bucket exists
if aws s3api head-bucket --bucket $S3_BUCKET_NAME 2>/dev/null; then
  echo "Found Bucket $S3_BUCKET_NAME."
else
  echo "Can't find $S3_BUCKET_NAME."
  exit 1
fi


echo "$S3_BUCKET_NAME -> $dest_dir"

if [ -n "$1" ] && [[ "$1" != *"shutdown"* ]]; then
  aws s3 sync s3://$S3_BUCKET_NAME $dest_dir --exclude backup.log | tee backup.log
else
  echo "Version $1"
  aws s3 sync s3://$S3_BUCKET_NAME $dest_dir --exclude backup.log --source-version-id $1 | tee backup.log
fi
